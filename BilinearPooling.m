classdef BilinearPooling < dagnn.Filter
  properties
    method = 'sum'
    normalizeGradients = false;
  end

  methods
    function outputs = forward(obj, inputs, params)
      outputs{1} = vl_nnbilinearpool(inputs{1});
    end

    function [derInputs, derParams] = backward(obj, inputs, params, derOutputs)
      derInputs{1} = vl_nnbilinearpool(inputs{1}, derOutputs{1});
      if obj.normalizeGradients,
        gradNorm = sum(abs(derInputs{1}(:))) + 1e-8;
        derInputs{1} = derInputs{1}/gradNorm;
      end
      derParams = {} ;
    end
    
    function rfs = getReceptiveFields(obj)
      rfs(1,1).size = [NaN NaN] ;
      rfs(1,1).stride = [NaN NaN] ;
      rfs(1,1).offset = [NaN NaN] ;
      rfs(2,1) = rfs(1,1) ;
    end

    function obj = BilinearPooling(varargin)
      obj.load(varargin) ;
    end
  end
end

